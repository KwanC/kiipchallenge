//
//  ViewController.m
//  KMCApp
//
//  Created by Kwan Cheng on 2/17/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    GKHackerNews* gkhn;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    gkhn = [[GKHackerNews alloc]init];
    gkhn.delegate = self;
    [gkhn initSDK];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// MARK: GKHackerNewsDelegate
-(void) topStoriesListAvailable {
    [gkhn showHackerNews];
}
@end
