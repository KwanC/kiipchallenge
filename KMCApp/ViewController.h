//
//  ViewController.h
//  KMCApp
//
//  Created by Kwan Cheng on 2/17/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKHackerNewsApi/GKHackerNewsApi.h>

@interface ViewController : UIViewController<GKHackerNewsDelegate>
@end

